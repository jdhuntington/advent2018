package main

import (
	"log"
	"strings"

	internal "repo.jdhuntington.com/jedediah/advent2018/internal"
)

func process(line string) {
	changed := true
	for {
		changed = false
		for i := 0; i < len(line)-1; i++ {
			char0 := line[i : i+1]
			char1 := line[i+1 : i+2]
			if char0 != char1 && strings.ToLower(char0) == strings.ToLower(char1) {
				changed = true
				line = internal.Splice(line, i, 2)
				i--
			}
		}
		if !changed {
			break
		}
	}
	log.Println("line length = ", len(line))
	if len(line) < 100 {
		log.Println("line = ", line)
	}
}

func main() {
	for l := range internal.ReadStdin() {
		process(l)
	}
}
