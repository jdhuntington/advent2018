package main

import (
	"log"
	"regexp"
	"strconv"
	"time"

	internal "repo.jdhuntington.com/jedediah/advent2018/internal"
)

const wakeup string = "wakes up"
const asleep string = "falls asleep"
const begin string = "begin"
const guardMessage string = "Guard #"
const layout string = "[2006-01-02 15:04]" //[1518-02-17 00:33]

func parseLog(l string) (time.Time, int, string) {
	timePart := l[:18]
	t, err := time.Parse(layout, timePart)
	if err != nil {
		log.Fatalln(err)
	}
	messagePart := l[19:]
	if messagePart[:7] == guardMessage {
		re := regexp.MustCompile(`Guard #(?P<guardno>\d+)`)
		matches := re.FindStringSubmatch(messagePart)
		guardNumberAsString := matches[1]
		guardNumber, err := strconv.Atoi(guardNumberAsString)
		if err != nil {
			log.Fatalln(err)
		}
		return t, guardNumber, begin
	}
	return t, 0, messagePart
}

func main() {
	sleepingTimes := make(map[int][]int)
	currentGuard := 0
	isAsleep := false
	var sleepStart time.Time
	for l := range internal.ReadStdin() {
		timestamp, guardNumber, message := parseLog(l)
		if message == begin {
			currentGuard = guardNumber
		} else if message == asleep {
			if isAsleep {
				log.Fatalln("Falling asleep, but already asleep!")
			}
			isAsleep = true
			sleepStart = timestamp
		} else if message == wakeup {
			if !isAsleep {
				log.Fatalln("Waking up, but not sleeping!")
			}
			isAsleep = false
			for currentTime := sleepStart; currentTime.Before(timestamp); {
				sleepingTimes[currentGuard] = append(sleepingTimes[currentGuard], currentTime.Minute())
				currentTime = currentTime.Add(time.Minute)
			}
		} else {
			log.Fatalln("unknown message", l, message)
		}
	}

	maxLength := -1
	maxAnswer := -1
	for k, v := range sleepingTimes {
		// log.Println("k = ", k, "  v = ", v)
		minutesSlept := make(map[int]int)
		sleepLength := 0
		for _, slot := range v {
			sleepLength++
			minutesSlept[slot]++
		}
		answerCount := 0
		answerValue := -1
		for minuteNumber, minutesTotal := range minutesSlept {
			if minutesTotal > answerCount {
				answerCount = minutesTotal
				answerValue = minuteNumber
			}
		}
		/* problem 1
		if sleepLength > maxLength {
			// log.Println("Guard", k, "slept", sleepLength, "minutes")
			maxLength = sleepLength
			maxAnswer = k * answerValue
		} */
		/* problem 2 */
		if answerCount > maxLength {
			log.Println("Guard", k, "spent", answerCount, "minutes asleep at minute marker", answerValue)
			maxLength = answerCount
			maxAnswer = k * answerValue
		}
	}
	log.Println(maxAnswer)
}
