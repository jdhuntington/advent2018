package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func rudimentaryChecksum(text string) (bool, bool) {
	hasTwo := false
	hasThree := false
	seen := make(map[string]int)
	for i := 0; i < len(text); i++ {
		seen[text[i:i+1]]++
	}
	for _, v := range seen {
		if v == 2 {
			hasTwo = true
		}
		if v == 3 {
			hasThree = true
		}
	}
	return hasTwo, hasThree
}

func checksum() {
	hasTwoLetter := 0
	hasThreeLetter := 0
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		text := scanner.Text()
		twoMatch, threeMatch := rudimentaryChecksum(text)
		if twoMatch {
			hasTwoLetter++
		}
		if threeMatch {
			hasThreeLetter++
		}
	}
	if err := scanner.Err(); err != nil {
		log.Println(err)
	}
	result := hasThreeLetter * hasTwoLetter
	fmt.Println("hasTwo: ", hasTwoLetter, " hasThree: ", hasThreeLetter)
	fmt.Println(result)
}

func splice(text string, drop int) string {
	return (text[:drop] + text[drop+1:])
}

func findMatch() {
	seen := make(map[string]bool)
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		text := scanner.Text()
		seenThisString := make(map[string]bool)
		for i := 0; i < len(text); i++ {
			spliced := splice(text, i)
			// fmt.Println("Spliced string (", i, "/", len(text), ") =", spliced)
			if seen[spliced] && !seenThisString[spliced] {
				fmt.Println(spliced)
			}
			seen[spliced] = true
			seenThisString[spliced] = true
		}
	}
	if err := scanner.Err(); err != nil {
		log.Println(err)
	}
}

func main() {
	if len(os.Args) != 2 {
		log.Fatalln("Expected 1 argument, got ", len(os.Args)-1)
	}
	if os.Args[1] == "checksum" {
		checksum()
	}
	if os.Args[1] == "findmatch" {
		findMatch()
	}

}
