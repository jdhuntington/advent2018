input = ARGF.read.split("\n")

used = Hash.new 0

input.each do |txt|
  p = txt.split(/[, @:#x]/).map(&:to_i)
  (p[4]...(p[4]+p[7])).to_a.each do |x|
    (p[5]...(p[5]+p[8])).to_a.each do |y|
      used[[y,x]] += 1
    end
  end
end

p used.values.count { |x| x>1 }
