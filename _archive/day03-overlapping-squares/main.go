package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	_ "regexp/syntax"
	"strconv"
)

func readStdin() <-chan string {
	messages := make(chan string, 2)
	go func() {
		defer close(messages)
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			text := scanner.Text()
			messages <- text
		}
		if err := scanner.Err(); err != nil {
			log.Println(err)
		}
	}()
	return messages
}

type fabricCut struct {
	id     string
	left   int
	top    int
	width  int
	height int
}

var re *regexp.Regexp

// #113 @ 464,514: 11x18
func parseCut(line string) fabricCut {
	s := fabricCut{}
	matches := re.FindStringSubmatch(line)
	s.id = matches[1]
	s.left, _ = strconv.Atoi(matches[2])
	s.top, _ = strconv.Atoi(matches[3])
	s.width, _ = strconv.Atoi(matches[4])
	s.height, _ = strconv.Atoi(matches[5])
	return s
}

func applyCut(cutDetails fabricCut, area [][]string, valid map[string]bool) {
	thisCutValid := true
	for row := cutDetails.top; row < cutDetails.top+cutDetails.height; row++ {
		for col := cutDetails.left; col < cutDetails.left+cutDetails.width; col++ {
			if area[row][col] == "" {
				area[row][col] = cutDetails.id
			} else {
				valid[area[row][col]] = false
				thisCutValid = false
			}
		}
	}
	valid[cutDetails.id] = thisCutValid
}

func main() {
	size := 1000
	re = regexp.MustCompile(`(?P<name>.+)\ @\ (?P<left>\d+),(?P<top>\d+):\ (?P<width>\d+)x(?P<height>\d+)`)
	valid := make(map[string]bool)
	area := make([][]string, size)
	for i := 0; i < size; i++ {
		area[i] = make([]string, size)
	}
	for l := range readStdin() {
		cut := parseCut(l)
		applyCut(cut, area, valid)
	}
	for k, v := range valid {
		if v {
			fmt.Println(k)
		}
	}
}
