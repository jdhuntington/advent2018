package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func applyFrequencyChanges(changes []int, start int) int {
	seen := make(map[int]bool)
	seen[start] = true
	for i := 0; ; i++ {
		start += changes[i%len(changes)]
		if seen[start] {
			fmt.Println(i)
			return start
		}
		seen[start] = true
	}
}

func main() {
	if len(os.Args) != 2 {
		log.Fatalln("Expected 1 argument, got ", len(os.Args)-1)
	}
	startingFrequency, err := strconv.Atoi(os.Args[1])
	if err != nil {
		log.Fatalln("Unable to process arg, please pass starting frequency.")
	}
	frequencyChanges := []int{}
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		text := scanner.Text()
		i, err := strconv.Atoi(text)
		if err != nil {
			log.Fatalln("Bad value = ", text)
		}
		frequencyChanges = append(frequencyChanges, i)
	}
	if err := scanner.Err(); err != nil {
		log.Println(err)
	}
	endingFrequency := applyFrequencyChanges(frequencyChanges, startingFrequency)
	fmt.Println(endingFrequency)
}
