package internal

import (
	"bufio"
	"log"
	"os"
)

// ReadStdin returns a buffered channel containing all lines read on STDIN
func ReadStdin() <-chan string {
	messages := make(chan string, 2)
	go func() {
		defer close(messages)
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			text := scanner.Text()
			messages <- text
		}
		if err := scanner.Err(); err != nil {
			log.Println(err)
		}
	}()
	return messages
}
