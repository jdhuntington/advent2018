package internal

// Splice returns a new string with count characters removed from index
func Splice(text string, index int, count int) string {
	return (text[:index] + text[index+count:])
}
